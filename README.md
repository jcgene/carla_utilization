# Carla Simulator

## Contents

- [Installation of CARLA - Linux Build](#installation-of-carla---linux-build)
  - [1. Preparation](#1-preparation)
    - [1.1. Dependencies](#11-dependencies)
    - [1.2. Default Version of Clang](#12-default-version-of-clang)
    - [1.3. Port Configuration](#13-port-configuration)
    - [1.4. Unreal Engine](#14-unreal-engine)
  - [2. CARLA Build](#2-carla-build)
    - [2.1. Clone Repo](#21-clone-repo)
    - [2.2. Get Assets](#22-get-assets)
    - [2.3. Set the Environment Variable](#23-set-the-environment-variable)
    - [2.4. Make CARLA](#24-make-carla)
- [Installation of ROS Bridge](#installation-of-ros-bridge)
  - [1. Setup for Debian Repository](#1-setup-for-debian-repository)
  - [2. Installation](#2-installation)
- [Run CARLA](#run-carla)
  - [1. Start CARLA](#1-start-carla)
  - [2. Spawn NPC](#2-spawn-npc)
  - [3. Create an Actor](#3-create-an-actor)
  - [4. Start ROS Bridge](#4-start-ros-bridge)

---

## Installation of CARLA - Linux Build

> - Click [here](https://carla.readthedocs.io/en/latest/build_linux/) for more details of Linux build.
> - A quick-start installation through pre-packaged version of CARLA is also available as shown in this [link](https://carla.readthedocs.io/en/latest/start_quickstart/). However, it is not used here due to the incompatibility between ROS-Melodic and the latest version of CARLA (above version 0.9.10). If you want the latest version of CARLA to work with ROS-Melodic, a build of PythonAPI with Python2 is required. Otherwise, a previous version of CARLA should be installed.

### 1. Preparation

#### 1.1. Dependencies

```bash
sudo apt-get update
sudo apt-get install wget software-properties-common
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key|sudo apt-key add -
sudo apt-add-repository "deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-8 main"
sudo apt-get update
```

- For Ubuntu 18.04:

```bash
sudo apt-get install build-essential clang-8 lld-8 g++-7 cmake
sudo apt-get install ninja-build libvulkan1 python python-pip
sudo apt-get install python-dev python3-dev python3-pip libpng-dev
sudo apt-get install libtiff5-dev libjpeg-dev tzdata sed curl unzip
sudo apt-get install autoconf libtool rsync libxml2-dev
sudo apt-get install aria2
pip2 install --user setuptools
pip3 install --user -Iv setuptools==47.3.1
pip2 install --user distro
pip3 install --user distro
```

#### 1.2. Default Version of Clang

```bash
sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/lib/llvm-8/bin/clang++ 180
sudo update-alternatives --install /usr/bin/clang clang /usr/lib/llvm-8/bin/clang 180
```

#### 1.3. Port Configuration

```bash
iptables -I INPUT -p tcp --dport 2000 -j ACCEPT
iptables -I INPUT -p tcp --dport 2001 -j ACCEPT
iptables-save
```

#### 1.4. Unreal Engine

Follw this [guide](https://docs.unrealengine.com/en-US/Platforms/Linux/BeginnerLinuxDeveloper/SettingUpAnUnrealWorkflow/index.html) to build UE in Ubuntu. Notice that CARLA will require **UE 4.24 release**, not the latest one.

### 2. CARLA Build

#### 2.1. Clone Repo

```bash
cd ${INSTALLATION_PATH_YOU_LIKE}
git clone https://github.com/carla-simulator/carla
```

#### 2.2. Get Assets

```bash
cd ${INSTALLATION_PATH_OF_CARLA}/carla
./Update.sh
```

#### 2.3. Set the Environment Variable

Add the following command to ~/.bashrc

```bash
export UE4_ROOT=${YOUR_PATH_TO_UE}/UnrealEngine_4.24
```

#### 2.4. Make CARLA

```bash
cd ${INSTALLATION_PATH_OF_CARLA}/carla
make PythonAPI ARGS="--python-version=2"
make launch
```

---

## Installation of ROS Bridge

> [Here](https://carla.readthedocs.io/en/latest/ros_installation/) is the official guide to this installation.

### 1. Setup for Debian Repository

```bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1AF1527DE64CB8D9
sudo add-apt-repository "deb [arch=amd64] http://dist.carla.org/carla $(lsb_release -sc) main"
```

### 2. Installation

```bash
sudo apt-get update # Update the Debian package index
sudo apt-get install carla-ros-bridge # Install the latest ROS bridge version, or update the current installation
```

---

## Run CARLA

### 1. Start CARLA

```bash
cd ${INSTALLATION_PATH_OF_CARLA}/carla
make launch
```

Wait until UE is runing and all the features are compiled. Click the button ”Play“ to start the virtual environment.

<div  align="center">
 <img src="./Pictures/start_env1.png" alt="Start the Environment" align=center />
</div>

### 2. Spawn NPC

```bash
cd ${INSTALLATION_PATH_OF_CARLA}/carla
cd PythonAPI/examples
python spawn_npc.py -n NUMBER_OF_VEHICLES -w NUMBER_OF_PEDESTRIANS
```

### 3. Create an Actor

```bash
cd ${INSTALLATION_PATH_OF_CARLA}/carla
cd PythonAPI/examples
python manual_control.py --rolename NAME
```

Inside the window of Pygame, you can press 'P' to enable auto-pilot and press 'R' to start recording. The recorded frames are saved in "PythonAPI/examples/_out/" as .png files. Press 'H' for more possible operations.

<div  align="center">
 <img src="./Pictures/pygame.png" alt="pygame" align=center />
</div>

### 4. Start ROS Bridge

```bash
roslaunch carla_ros_bridge carla_ros_bridge.launch
```
